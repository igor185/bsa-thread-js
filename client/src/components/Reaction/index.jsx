import React from 'react';
import {Icon, Image, Label, Popup} from 'semantic-ui-react'
import {getUserImgLink} from '../../helpers/imageHelper';
import {findByKey} from '../../helpers/find.helpers'
import configReaction from './configReaction';

const Reaction = ({reactions, selfID, setReaction, styles, type}) => {
    let liked = findByKey(reactions, 'userId', selfID) !== -1 ? configReaction[type].class : '';
    return (
        <Popup trigger={
            <Label basic size="small" as="a" className={styles.toolbarBtn + liked}
                   onClick={setReaction}>
                <Icon name={configReaction[type].icon}/>
                {reactions.length}
            </Label>}>
            <Popup.Content>
                {reactions.length > 0 ? reactions.map(elem => {
                    return <Image src={getUserImgLink(elem)} avatar key={elem.id}/>
                }) : configReaction[type].noReactions}
            </Popup.Content>
        </Popup>
    );
};

export default Reaction;
