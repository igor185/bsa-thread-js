import React from 'react';
import PropTypes from 'prop-types';
import {Card, Icon, Image, Label} from 'semantic-ui-react';
import moment from 'moment';
import Reaction from '../Reaction'

import styles from './styles.module.scss';


class Post extends React.Component {

    render() {
        const hide = this.props.post.hide ? " hiddenPost" : '';
        const post = this.props.post;
        const date = moment(post.updatedAt).fromNow();

        if (!post.user) {
            return '';
        }
        return (
            <Card style={{width: '100%'}}>
                {post.image && <Image src={post.image.link} wrapped ui={false}/>}
                <Card.Content>
                    <Card.Meta>
                        <span className="date">
                            posted by
                            {' '}
                            {post.user.username}
                            {' - '}
                            {date}
                        </span>
                    </Card.Meta>
                    <Card.Description>
                        {post.body}
                    </Card.Description>
                </Card.Content>
                <Card.Content extra>
                    <Reaction type={'like'} styles={styles}
                              setReaction={() => this.props.reactPost(post.id, this.props.user, true, post.userId)}
                              reactions={post.likes} selfID={this.props.selfID}/>
                    <Reaction type={'dislike'} styles={styles}
                              setReaction={() => this.props.reactPost(post.id, this.props.user, false)}
                              reactions={post.dislikes} selfID={this.props.selfID}/>

                    <Label basic size="small" as="a" className={styles.toolbarBtn}
                           onClick={() => this.props.toggleExpandedPost(post.id)}>
                        <Icon name="comment"/>
                        {post.commentCount}
                    </Label>
                    <Label basic size="small" as="a" className={styles.toolbarBtn}
                           onClick={() => this.props.sharePost(post.id)}>
                        <Icon name="share alternate"/>
                    </Label>
                    {post.user && this.props.selfID === post.user.id ?
                        <span>
                            <Label basic size="small" as="a" className={styles.toolbarBtn}
                                   onClick={() => this.props.toggleUpdatePost(post)}>
                                <Icon name="write"/>
                            </Label>
                            { !post.delete ?
                                <Label basic size="small" as="a" className={styles.toolbarBtn}
                                       onClick={() => this.props.deletePost(post.id, this.props.selfID, true)}>
                                    <Icon name="trash"/>
                                </Label> :
                                <Label basic size="small" as="a" className={styles.toolbarBtn + ' deleted'}
                                       onClick={() => this.props.deletePost(post.id, this.props.selfID, false
                                       )}>
                                    <Icon name="trash"/>
                                </Label>}
                            <Label basic size="small" as="a" className={styles.toolbarBtn + hide}
                                   onClick={() => this.props.hidePost(post.id, !post.hide, post.userId)}>
                                <Icon name="hide"/>
                            </Label>
                        </span> : null}
                </Card.Content>
            </Card>
        );
    };
}


Post.propTypes = {
    post: PropTypes.objectOf(PropTypes.any).isRequired,
    toggleExpandedPost: PropTypes.func.isRequired,
    sharePost: PropTypes.func.isRequired
};

export default Post;
