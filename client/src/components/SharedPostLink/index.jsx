import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Input, Icon } from 'semantic-ui-react';

import styles from './styles.module.scss';
import isEmail from 'validator/lib/isEmail';
import { NotificationManager } from 'react-notifications';
import { sharePost } from '../../services/postService'

class SharedPostLink extends React.Component {
    state = {
        copied: false,
        valid: false,
        email: ''
    };

    copyToClipboard = (e) => {
        this.input.select();
        document.execCommand('copy');
        e.target.focus();
        this.setState({ copied: true });
    };

    render() {
        const { postId, close, selfMail } = this.props;
        const { copied } = this.state;
        return (
            <Modal open onClose={close}>
                <Modal.Header className={styles.header}>
                    <span>Share Post</span>
                    {copied && (
                        <span>
                            <Icon color="green" name="copy" />
                            Copied
                        </span>
                    )}
                </Modal.Header>
                <Modal.Content>
                    <Input
                        fluid
                        action={{ color: 'teal', labelPosition: 'right', icon: 'copy', content: 'Copy', onClick: this.copyToClipboard }}
                        value={`${window.location.origin}/share/${postId}`}
                        ref={(input) => { this.input = input; }}
                    />
                    <br />
                    <span>...or you can send post to email</span>
                    <br />
                    <br />
                    <Input
                        fluid
                        action={{
                            color: 'teal', labelPosition: 'right', icon: 'mail', content: 'Send', onClick: () => {
                                if (!this.state.valid) {
                                    NotificationManager.error('Email is not valid')
                                    return;
                                }
                                sharePost({getMail: this.state.email, sendMail: selfMail, postHash:`${window.location.origin}/share/${postId}`})
                                    .then(() => NotificationManager.info('Email was send!'))
                                    .catch((error) => NotificationManager.error(error.message, "Something wrong!"))
                            }
                        }}
                        placeholder={"write mail recipient"}
                        value={this.state.email}
                        error={!this.state.valid}
                        onChange={ev => this.emailChanged(ev.target.value)}
                    />
                </Modal.Content>
            </Modal>
        );
    }
    emailChanged = email => this.setState({ email, valid: this.validateEmail(email) });
    validateEmail = email => isEmail(email);
}

SharedPostLink.propTypes = {
    postId: PropTypes.string.isRequired,
    close: PropTypes.func.isRequired
};

export default SharedPostLink;
