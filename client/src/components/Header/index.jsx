import React from 'react';
import PropTypes from 'prop-types';
import {NavLink} from 'react-router-dom';
import {getUserImgLink} from 'src/helpers/imageHelper';
import {Button, Grid, Header as HeaderUI, Icon, Image} from 'semantic-ui-react';
import ChangeStatus from './ChangeStatus/index';
import styles from './styles.module.scss';

const Header = ({user, logout, postStatus, updateStatus}) => (
    <div className={styles.headerWrp}>
        <Grid centered container columns="2">
            <Grid.Column>
                {user && (
                    <HeaderUI>
                        <Image circular src={getUserImgLink(user.image)}/>
                        <HeaderUI.Content>
                            <NavLink exact to="/profile">
                                {user.username}
                            </NavLink>
                            <HeaderUI.Subheader>
                                <ChangeStatus postStatus={postStatus} user={user} updateStatus={updateStatus}/>
                            </HeaderUI.Subheader>
                        </HeaderUI.Content>
                    </HeaderUI>

                )}
            </Grid.Column>
            <Grid.Column textAlign="right">
                <NavLink exact activeClassName="active" to="/">
                    <Icon name="home" size="large"/>
                </NavLink>
                <Button basic icon type="button" className={styles.logoutBtn} onClick={logout}>
                    <Icon name="log out" size="large"/>
                </Button>
            </Grid.Column>
        </Grid>
    </div>
);

Header.propTypes = {
    logout: PropTypes.func.isRequired,
    user: PropTypes.objectOf(PropTypes.any).isRequired
};

export default Header;
