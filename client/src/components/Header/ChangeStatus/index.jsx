import React from 'react';
import {Input} from 'semantic-ui-react'

class ChangeStatus extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            status: props.user.status || '',
            changeMode: false
        };
    }

    render = () => this.state.changeMode ? this.renderInput(this.props) :
        <span style={{cursor: 'pointer'}} onClick={this.changeMode}>{this.props.user.status}</span>;

    renderInput(props) {
        const {postStatus, user, updateStatus} = props;
        return <Input
            type={'text'}
            autoFocus
            defaultValue={this.state.status}
            onBlur={this.changeMode}
            onKeyUp={event => {
                // console.log(event.target.value);
                this.setState({status: event.target.value});
                if (event.which === 13) {
                    // console.log(this.state.status);
                    postStatus(this.state.status, user.id);
                    updateStatus(this.state.status);
                    this.changeMode();
                }
            }}
        />
    }

    changeMode = () => this.setState((state) => {
        return {changeMode: !state.changeMode};
    });
}

export default ChangeStatus;
