import React from 'react';
import { Form, Button, Modal } from 'semantic-ui-react';

class UpdateComment extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            body: this.props.comment.body || '',
            open: true,
        };
    }
    closeModal = () => {
        this.props.toggleUpdateComment(undefined)
    }

    handleUpdateComment = async () => {
        const { body } = this.state;
        if (!body) {
            return;
        }
        this.closeModal();
        await this.props.updateComment(this.props.comment.id, this.props.comment, this.state.body, this.props.postIdForUpdateComment);

    }


    render() {
        const { body } = this.state;
        return (
            <Modal dimmer="blurring" centered={false} open={this.state.open} onClose={this.closeModal}>
                <Modal.Content>
                    <Form reply onSubmit={this.handleUpdateComment}>
                        <Form.TextArea
                            value={body}
                            placeholder="Type a comment..."
                            onChange={ev => this.setState({ body: ev.target.value })}
                        />
                        <Button type="submit" content="Update comment" labelPosition="left" icon="edit" primary />
                    </Form>
                </Modal.Content>
            </Modal>
        );
    }
}
export default UpdateComment;