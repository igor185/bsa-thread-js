import React from 'react';
import {Checkbox, Input, Tab} from "semantic-ui-react";
import moment from 'moment';


class Filters extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            sortByLikes: false,
            sortByDate: false,
            sortByComments: false
        }
    }

    render() {
        const panes = [
            {menuItem: 'Choose filter', render: () => this.chooseFilter(this.props)},
            {menuItem: 'Choose sort by', render: () => this.chooseSort(this.props)},
            {menuItem: 'Choose filter by amount', render: () => this.chooseAmount(this.props)},
        ];

        return (<Tab panes={panes}/>)
    }

    chooseFilter(props) {
        const {styles, showOwnPosts, togglePosts, addFilter, NotOwnFilter, likeFilter, dislikeFilter, deletedPost} = props;
        return (
            <Tab.Pane>
                <div className={"filters"}>
                    <div className={styles.toolbar}>
                        <Checkbox toggle label="Show only my posts" onChange={togglePosts}/>
                    </div>
                    <div className={styles.toolbar}>
                        <Checkbox toggle label="Don't show my posts" onChange={() => addFilter(NotOwnFilter)}/>
                    </div>
                    <div className={styles.toolbar}>
                        <Checkbox toggle label="Liked by me" onChange={() => addFilter(likeFilter)}/>
                    </div>
                    <div className={styles.toolbar}>
                        <Checkbox toggle label="Disliked by me" onChange={() => addFilter(dislikeFilter)}/>
                    </div>
                    <div className={styles.toolbar}>
                        <Checkbox toggle label="Deleted" onChange={deletedPost}/>
                    </div>
                </div>
            </Tab.Pane>
        )
    }

    chooseSort(props) {
        const {styles, sortByLikes, sortByDate, sortByComments, addSort} = props;
        return (
            <Tab.Pane>
                <div className={styles.toolbar}>
                    <Checkbox toggle label="Likes" checked={this.state.sortByLikes} onChange={() => {
                        if (this.state.sortByLikes)
                            this.setState({sortByLikes: false});
                        else
                            this.setState({sortByLikes: true, sortByDate: false, sortByComments: false});
                        addSort(sortByLikes)
                    }}/>
                </div>
                <div className={styles.toolbar}>
                    <Checkbox toggle label="Date" checked={this.state.sortByDate} onChange={() => {
                        if (this.state.sortByDate)
                            this.setState({sortByDate: false});
                        else
                            this.setState({sortByDate: true, sortByLikes: false, sortByComments: false});
                        addSort(sortByDate)
                    }}/>
                </div>
                <div className={styles.toolbar}>
                    <Checkbox toggle label="Comments" checked={this.state.sortByComments} onChange={() => {
                        if (this.state.sortByComments)
                            this.setState({sortByComments: false});
                        else
                            this.setState({sortByComments: true, sortByLikes: false, sortByDate: false});
                        addSort(sortByDate);
                        addSort(sortByComments);
                    }}/>
                </div>
            </Tab.Pane>
        )
    }

    chooseAmount(props) {
        const {styles, addFilter, amountLikeFilter, amountCountFilter, amountDateFilter, setAmount} = props;
        return (
            <Tab.Pane>
                <div className={styles.toolbar + ' amountChooser'}>
                    <Checkbox toggle label="Likes" onChange={() => addFilter(amountLikeFilter)}/>
                    <Input type='number' min={1} defaultValue={1} onChange={ev => {
                        let number = +ev.target.value;
                        if (number && number >= 1)
                            setAmount({type: 'likes', number});
                    }
                    }/>
                </div>
                <div className={styles.toolbar + ' amountChooser'}>
                    <Checkbox toggle label="Comments" onChange={() => addFilter(amountCountFilter)}/>
                    <Input type='number' min={1} defaultValue={1} onChange={ev => {
                        let number = +ev.target.value;
                        if (number && number >= 1)
                            setAmount({type: 'comments', number});
                    }}/>
                </div>
                <div className={styles.toolbar + ' amountChooser'}>
                    <Checkbox toggle label="Date" onChange={() => addFilter(amountDateFilter)}/>
                    <Input type='date' defaultValue={'2019-06-01'} onChange={ev => {
                        let value = ev.target.value;
                        if(value && "Invalid data" !== moment(value).toString())
                            setAmount({type: 'date', number:moment(value).toString()});
                    }}/>
                </div>
            </Tab.Pane>)
    }
}

export default Filters;
