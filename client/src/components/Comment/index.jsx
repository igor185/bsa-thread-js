import React from 'react';
import PropTypes from 'prop-types';
import {Comment as CommentUI, Icon, Label} from 'semantic-ui-react';
import moment from 'moment';
import {getUserImgLink} from '../../helpers/imageHelper';
import {filter} from '../../helpers/find.helpers';
import Reaction from '../Reaction';

import styles from './styles.module.scss';

const Comment = (props) => {
    const {comment: {body, createdAt, user}} = props;
    const date = moment(createdAt).fromNow();
    const {likes, dislikes} = filter(props.comment.react);
    const {toolbarBtn, comment} = styles;

    return (
        <CommentUI className={comment}>
            <CommentUI.Avatar src={getUserImgLink(user.image)}/>
            <CommentUI.Content>
                <CommentUI.Author as="a">
                    {user.username}
                </CommentUI.Author>
                <CommentUI.Metadata>
                    {user.status ? user.status + ' - ' : ''}
                    {date}
                </CommentUI.Metadata>
                <CommentUI.Text>
                    {body}
                </CommentUI.Text>
                <CommentUI.Actions>
                    <Reaction type={'like'} styles={styles}
                              setReaction={() => props.reactComment(props.comment.id, props.selfId, true)}
                              reactions={likes} selfID={props.selfId}/>
                    <Reaction type={'dislike'} styles={styles}
                              setReaction={() => props.reactComment(props.comment.id, props.selfId, false)}
                              reactions={dislikes} selfID={props.selfId}/>
                    {props.selfId === props.comment.userId ?
                        <span>
                            <Label basic size="small" as="a" className={toolbarBtn}
                                   onClick={() => props.toggleUpdateComment(props.comment, props.postId)}>
                                <Icon name="write"/>
                            </Label>
                            <Label basic size="small" as="a" className={toolbarBtn}
                                   onClick={() => props.deleteComment(props.comment.id, props.comment.postId)}>
                                <Icon name="trash"/>
                            </Label>
                        </span> : null
                    }
                </CommentUI.Actions>
            </CommentUI.Content>
        </CommentUI>
    );
};

Comment.propTypes = {
    comment: PropTypes.objectOf(PropTypes.any).isRequired
};

export default Comment;
