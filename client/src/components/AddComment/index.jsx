import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {Button, Form} from 'semantic-ui-react';

const AddComment = (props) => {

    const [body, setBody] = useState('');

    let handleAddComment = async () => {
        if (!body)
            return;

        await props.addComment({postId: props.postId, body});
        setBody('');
    };

    return (
        <Form reply onSubmit={handleAddComment}>
            <Form.TextArea
                value={body}
                placeholder="Type a comment..."
                onChange={ev => setBody(ev.target.value)}
            />
            <Button type="submit" content="Post comment" labelPosition="left" icon="edit" primary/>
        </Form>
    );
};

AddComment.propTypes = {
    addComment: PropTypes.func.isRequired,
    postId: PropTypes.string.isRequired
};

export default AddComment;
