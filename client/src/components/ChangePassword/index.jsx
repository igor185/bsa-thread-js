import React, {useState} from 'react';
import {Button, Form, Grid, Header, Message, Segment} from 'semantic-ui-react';
import {NavLink, Redirect} from "react-router-dom";
import Logo from "../Logo";

const ResetPassword = (props) => {
    const [message, setMessage] = useState('');

    const token = props.match.params.token;
    if(!token)
        props.history.push('/reset');



    if(!message)
        return (!props.isAuthorized
            ? (
                <Grid textAlign="center" verticalAlign="middle" className="fill">
                    <Grid.Column style={{maxWidth: 450}}>
                        <Logo/>
                        <Header as="h2" color="teal" textAlign="center">
                            Reset password
                        </Header>
                        <Form name="registrationForm" size="large">
                            <Segment>
                                <Form.Input
                                    id={"change-input"}
                                    fluid
                                    icon="at"
                                    iconPosition="left"
                                    placeholder="Password"
                                    type="password"
                                />
                                <Button type="submit" color="teal" fluid size="large" primary onClick={() => {
                                    const password = document.getElementById("change-input").value;
                                    console.log(password);
                                    props.changePassword(token, password)
                                        .then(() => props.history.push('/login'))
                                        .catch(e => setMessage(e.message));
                                }}>
                                    Reset
                                </Button>
                            </Segment>
                        </Form>
                        <Message>
                            Already with us?
                            {' '}
                            <NavLink exact to="/login">Sign In</NavLink>
                        </Message>
                    </Grid.Column>
                </Grid>
            )
            : <Redirect to="/"/>);
    else
        return message;
};

export default ResetPassword;
