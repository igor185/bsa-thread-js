import React from 'react';
import {Form, Button, Modal} from 'semantic-ui-react';

class UpdatePost extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            body: this.props.post.body || '',
            open: true,
        };
    }

    closeModal = () => {
        this.props.toggleUpdatePost(undefined)
    }
    handleUpdatePost = async () => {
        const {body} = this.state;
        if (!body) {
            return;
        }
        this.closeModal();
        await this.props.updatePost(this.props.post.id, this.props.post, this.state.body);

    }


    render() {
        const {body} = this.state;
        return (
            <Modal dimmer="blurring" centered={false} open={this.state.open} onClose={this.closeModal}>
                <Modal.Content>
                    <Form reply onSubmit={this.handleUpdatePost}>
                        <Form.TextArea
                            value={body}
                            placeholder="Type a post..."
                            onChange={ev => this.setState({body: ev.target.value})}
                        />
                        <Button type="submit" content="Update post" labelPosition="left" icon="edit" primary/>
                    </Form>
                </Modal.Content>
            </Modal>
        );
    }
}

export default UpdatePost;
