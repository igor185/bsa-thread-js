import callWebApi from 'src/helpers/webApiHelper';
import axios from 'axios';

export const getAllPosts = async (filter, userId) => {
    filter.selfId = userId;
    const response = await callWebApi({
        endpoint: '/api/posts',
        type: 'GET',
        query: filter
    });

    return response.json();
};

export const addPost = async (request) => {
    const response = await callWebApi({
        endpoint: '/api/posts',
        type: 'POST',
        request
    });
    return response.json();
};


export const getPost = async (id) => {
    const response = await callWebApi({
        endpoint: `/api/posts/${id}`,
        type: 'GET'
    });
    return response.json();
};


export const reactPost = async (postId, user, isLike,post_userId) => {
    const response = await callWebApi({
        endpoint: '/api/posts/reaction',
        type: 'PUT',
        request: {
            postId,
            isLike,
            user,
            post_userId
        }
    });
    return response.json();
};


export const deletePost = async (postId, userId, deletedPost) => {
    const response = await axios.delete(`/api/posts/${postId}`, {headers:{Authorization: `Bearer ${localStorage.getItem('token')}`}, params:{deletePost:deletedPost} });

    return response;
};

export const hidePost = async (postId, data) => {
    await callWebApi({
        endpoint: '/api/posts',
        type: 'PUT',
        request: {
            postId,
            data
        }
    });
};
export const sharePost = async (data) => {
    return await callWebApi({
        endpoint: '/api/posts/share',
        type: 'POST',
        request: data
    });
};

export const updatePost = async (id, post) => {
    return await callWebApi({
        endpoint: `/api/posts/${id}`,
        type: 'PUT',
        request: {
            post
        }
    });
};
export const updateComment = async (id, comment) => {
    return await callWebApi({
        endpoint: `/api/comments/${id}`,
        type: 'PUT',
        request: {
            comment
        }
    });
};
export const reactComment = async (data) => {
    const res = await callWebApi({
        endpoint: `/api/comments/react`,
        type: 'POST',
        request: data
    });
    return res.json();
};

export const deleteComment = async id => {
    return await callWebApi({
        endpoint: `/api/comments/${id}`,
        type: 'DELETE'
    });
};

// should be replaced by approppriate function
export const getPostByHash = async hash => getPost(hash);
