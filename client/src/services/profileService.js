import callWebApi from 'src/helpers/webApiHelper';

export const postName = async (name, id) => {
    const response = await callWebApi({
        endpoint: '/api/auth/user/username',
        type: 'POST',
        request: {
            name,
            id
        }
    });
    return response.json();
};

export const postStatus = async (status, id) => {
    await callWebApi({
        endpoint: '/api/auth/user/status',
        type: 'POST',
        request: {
            status,
            id
        }
    });
};
export const postImage = async (imageId, userId) => {
    await callWebApi({
        endpoint: '/api/auth/user/image',
        type: 'POST',
        request: {
            imageId,
            userId
        }
    });
};
