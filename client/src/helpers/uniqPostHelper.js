export const uniqPost = (posts) => {
    const map = new Map();
    for (let i = 0; i < posts.length; i++)
        map.set(posts[i].id, posts[i]);

    return Array.from(map.values())
};
