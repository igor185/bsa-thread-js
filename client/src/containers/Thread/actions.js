import * as postService from 'src/services/postService';
import * as commentService from '../../services/commentService';
import * as helper from '../../helpers/find.helpers.js'
import {ADD_POST, DEL_POST, LOAD_MORE_POSTS, SET_ALL_POSTS, SET_EXPANDED_POST} from './actionTypes';


const setPostsAction = posts => ({
    type: SET_ALL_POSTS,
    posts
});

const addMorePostsAction = posts => ({
    type: LOAD_MORE_POSTS,
    posts
});

const addPostAction = post => ({
    type: ADD_POST,
    post
});

const setExpandedPostAction = post => ({
    type: SET_EXPANDED_POST,
    post
});

const delPost = postId => ({
    type: DEL_POST,
    postId
});

export const loadPosts = (filter, userId) => async (dispatch) => {
    const posts = await postService.getAllPosts(filter, userId);
    dispatch(setPostsAction(posts));
};

export const loadMorePosts = (filter, userId) => async (dispatch) => {
    const posts = await postService.getAllPosts(filter, userId);
    dispatch(addMorePostsAction(posts));
};

export const applyPost = (postId) => async (dispatch) => {
    const post = await postService.getPost(postId);
    dispatch(addPostAction(post));
};

export const addPost = post => async (dispatch) => {
    const {id} = await postService.addPost(post);
    const newPost = await postService.getPost(id);
    if (!newPost.react)
        newPost.react = [];
    dispatch(addPostAction(newPost));
};

export const toggleExpandedPost = postId => async (dispatch) => {
    const post = postId ? await postService.getPost(postId) : undefined;
    dispatch(setExpandedPostAction(post));
};

export const reactPost = (postId, user, reaction, post_userId) => async (dispatch, getRootState) => {
    let reactions = await postService.reactPost(postId, user, reaction, post_userId);

    const {
        posts: {
            posts,
            expandedPost
        }
    } = getRootState();
    let all_posts = [...posts];

    let j = helper.findByKey(all_posts, 'id', postId);
    all_posts[j].react = reactions;
    dispatch({
        type: 'POST_ACTION:SET_ALL_POSTS',
        posts: all_posts
    });
    if (expandedPost && expandedPost.id === postId) {
        expandedPost.react = reactions;
        dispatch(setExpandedPostAction(expandedPost));
    }
};
export const addComment = request => async (dispatch, getRootState) => {
    const comment = await commentService.addComment(request);


    const mapComments = post => ({
        ...post,
        commentCount: Number(post.commentCount) + 1,
        comments: [...(post.comments || []), comment]
    });

    const {
        posts: {
            posts,
            expandedPost
        }
    } = getRootState();
    const updated = posts.map(post => (post.id !== comment.postId ?
        post :
        mapComments(post)));

    dispatch(setPostsAction(updated));

    if (expandedPost && expandedPost.id === comment.postId) {
        dispatch(setExpandedPostAction(mapComments(expandedPost)));
    }
};

export const deletePost = (postID, userID, deletePost) => async (dispatch, getRootState) => {
    await postService.deletePost(postID, userID, deletePost);
    dispatch(delPost(postID))
};

export const deletePostFromList = (postID) => async (dispatch, getRootState) => {
    await dispatch(delPost(postID));

    const {
        posts: {
            expandedPost
        }
    } = getRootState();

    if (expandedPost && expandedPost.id === postID) {
        dispatch(setExpandedPostAction(undefined))
    }
};

export const applyReaction = (reactions, postId) => async (dispatch, getRootState) => {
    const {
        posts: {
            posts,
            expandedPost
        }
    } = getRootState();
    let all_posts = [...posts];

    console.log(all_posts);
    console.log(postId);
    let j = helper.findByKey(all_posts, 'id', postId);
    console.log(j);
    all_posts[j].react = reactions;
    dispatch({
        type: 'POST_ACTION:SET_ALL_POSTS',
        posts: all_posts
    });
    if (expandedPost && expandedPost.id === postId) {
        expandedPost.react = reactions;
        dispatch(setExpandedPostAction(expandedPost));
    }
};
export const hidePost = (postId, hide, userId) => async (dispatch, getRootState) => {
    postService.hidePost(postId, {
        hide,
        userId
    });
    const {
        posts: {
            posts,
            expandedPost
        }
    } = getRootState();

    for (let i = 0; i < posts.length; i++) {
        if (posts[i].id === postId) {
            posts[i].hide = hide;
            dispatch(setPostsAction(posts));
            break;
        }
    }
    if (expandedPost && expandedPost.id === postId) {
        expandedPost.hide = hide;
        dispatch(setExpandedPostAction(expandedPost));
    }
};

export const sharePost = async (getMail, sendMail, postHash) => await postService.sharePost({
    getMail,
    sendMail,
    postHash
});

export const updatePost = (id, post, body) => async (dispatch, getRootState) => {
    post = {
        ...post,
        body
    };
    await postService.updatePost(id, post);
    dispatch({
        type: 'updatePost',
        post
    });
    const {
        posts: {
            expandedPost
        }
    } = getRootState();
    if (expandedPost && expandedPost.id === post.id)
        dispatch({
            type: SET_EXPANDED_POST,
            post
        });
};
export const updateComment = (id, comment, body, postId) => async (dispatch, getRootState) => {
    comment = {
        ...comment,
        body
    };
    await postService.updateComment(id, comment);
    const {
        posts: {
            expandedPost
        }
    } = getRootState();
    if (expandedPost && expandedPost.id === postId) {
        let comments = expandedPost.comments;
        let index = helper.findByKey(comments, 'id', id);
        comments[index] = comment;
        expandedPost.comments = comments;
        dispatch({
            type: SET_EXPANDED_POST,
            post: expandedPost
        });
    }
};

export const reactComment = (commentId, userId, react) => async (dispatch, getRootState) => {
    let reactions = await postService.reactComment({commentId, userId, react});

    const {
        posts: {
            expandedPost
        }
    } = getRootState();
    let post = {
        ...expandedPost
    };
    let comments = post.comments;

    let j = helper.findByKey(comments, 'id', commentId);
    comments[j].react = reactions;
    dispatch({
        type: 'POST_ACTION:SET_EXPANDED_POST',
        post: post
    });
};
export const deleteComment = (id, postId) => async (dispatch, getRootState) => {
    await postService.deleteComment(id);

    const {
        posts: {
            posts,
            expandedPost
        }
    } = getRootState();

    if (expandedPost && expandedPost.id === postId) {
        let comments = expandedPost.comments;
        let index = helper.findByKey(comments, 'id', id);
        comments.splice(index, 1);
        expandedPost.comments = comments;
        expandedPost.commentCount--;
        dispatch({
            type: SET_EXPANDED_POST,
            post: expandedPost
        });
    }
    for (let i = 0; i < posts.length; i++)
        if (posts[i] === postId) {
            posts[i].commentCount--;
            dispatch(setPostsAction(posts));
        }
};

