import React from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import * as imageService from 'src/services/imageService';
import ExpandedPost from 'src/containers/ExpandedPost';
import Post from 'src/components/Post';
import AddPost from 'src/components/AddPost';
import SharedPostLink from 'src/components/SharedPostLink';
import {Loader} from 'semantic-ui-react';
import InfiniteScroll from 'react-infinite-scroller';
import {
    addPost,
    deletePost,
    hidePost,
    loadMorePosts,
    loadPosts,
    reactPost,
    sharePost,
    toggleExpandedPost,
    updateComment,
    updatePost
} from './actions';
import UpdatePost from '../../components/UpdatePost/index'
import UpdateComment from '../../components/UpdateComment/index'
import Filters from '../../components/Filters'
import styles from './styles.module.scss';
import {filter} from "../../helpers/find.helpers";
import moment from 'moment';

class Thread extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sharedPostId: undefined,
            updatePost: undefined,
            updateComment: undefined,
            postIdForUpdateComment: undefined,
            showOwnPosts: false,
            posts: props.posts,
            filters: [],
            sort: null,
            amountForLikes: 1,
            amountForComments: 1,
            amountForDate: moment('2019-06-01'),
        };
        this.postsFilter = {
            userId: undefined,
            from: 0,
            count: 10,
            deletePost: false
        };
    }

    togglePosts = () => {
        this.setState(
            ({showOwnPosts}) => ({showOwnPosts: !showOwnPosts}),
            () => {
                Object.assign(this.postsFilter, {
                    ...this.postsFilter,
                    userId: this.state.showOwnPosts ? this.props.userId : undefined,
                    from: 0
                });
                this.props.loadPosts(this.postsFilter, this.props.userId);
            }
        );
    };
    deletedPost = () => {
        this.setState(
            ({showOwnPosts}) => ({showOwnPosts: !showOwnPosts}),
            () => {
                Object.assign(this.postsFilter, {
                    ...this.postsFilter,
                    userId: this.state.showOwnPosts ? this.props.userId : undefined,
                    from: 0,
                    deletePost: !this.postsFilter.deletePost
                });
                this.props.loadPosts(this.postsFilter, this.props.userId);
            }
        );
    };
    likeFilter = (post) => {
        const user = this.props.userId;
        return post.react.some((elem => elem.isLike && elem.userId === user));
    };
    dislikeFilter = (post) => {
        const user = this.props.userId;

        return post.react.some((elem => !elem.isLike && elem.userId === user));
    };
    NotOwnFilter = (post) => {
        const user = this.props.userId;

        return post.userId !== user;
    };
    amountLikeFilter = (post) => {
        return post.likes && (post.likes.length >= this.state.amountForLikes);
    };
    amountCountFilter = (post) => {
        return post.commentCount >= this.state.amountForComments;
    };
    amountDateFilter = (post) => {
        return moment(post.updatedAt).isAfter(this.state.amountForDate);
    };
    setAmount = ({type, number}) => {
        switch (type) {
            case 'likes':
                this.setState({amountForLikes: number});
                break;
            case 'comments':
                this.setState({amountForComments: number});
                break;
            case 'date':
                this.setState({amountForDate: number});
                break;
            default:
        }
    };
    addFilter = (filter) => {
        let index = this.state.filters.indexOf(filter);
        if (index === -1)
            this.setState({filters: [...this.state.filters, filter]});
        else {
            let arr = [...this.state.filters];
            arr.splice(index, 1);
            this.setState({filters: [...arr]})
        }
    };
    addSort = (sort) => {
        let index = this.state.sort;
        if (index === sort)
            this.setState({sort: null});
        else {
            this.setState({sort});
        }
    };
    loadMorePosts = () => {
        this.props.loadMorePosts(this.postsFilter, this.props.userId);
        const {from, count} = this.postsFilter;
        this.postsFilter.from = from + count;
    };
    sharePost = (sharedPostId) => {
        this.setState({sharedPostId});
    };
    closeSharePost = () => {
        this.setState({sharedPostId: undefined});
    };
    filtrate = (arr) => {
        const filters = this.state.filters;
        return arr.filter(elem => filters.every(filter => filter(elem)));
    };
    toggleUpdatePost = (post) => this.setState({updatePost: post});
    toggleUpdateComment = (comment, postId) => this.setState({updateComment: comment, postIdForUpdateComment: postId});
    uploadImage = file => imageService.uploadImage(file);
    sortByLikes = (post1, post2) => post2.likes.length - post1.likes.length;
    sortByDate = (post1, post2) => {
        return post1.updatedAt > post2.updatedAt ? 1 : post1.updatedAt < post2.updatedAt ? -1 : 0;
    };
    sortByComments = (post1, post2) => post2.commentCount - post1.commentCount;
    sortPost = arr => this.state.sort ? arr.sort(this.state.sort) : arr;

    render() {
        let {posts = [], expandedPost, hasMorePosts, ...props} = this.props;
        const selfID = this.props.userId;
        const {showOwnPosts, sharedPostId} = this.state;
        posts = posts.map(post => {
            const {likes, dislikes} = filter(post.react);
            post.likes = likes;
            post.dislikes = dislikes;
            return post;
        });


        let postsFiltrate = this.filtrate(posts);
        postsFiltrate = this.sortPost(postsFiltrate);

        return (
            <div className={styles.threadContent}>
                <div className={styles.addPostForm}>
                    <AddPost addPost={props.addPost} uploadImage={this.uploadImage}/>
                </div>
                <Filters styles={styles}
                         showOwnPosts={showOwnPosts}
                         togglePosts={this.togglePosts}
                         deletedPost={this.deletedPost}
                         addFilter={this.addFilter}
                         NotOwnFilter={this.NotOwnFilter}
                         likeFilter={this.likeFilter}
                         dislikeFilter={this.dislikeFilter}
                         sortByLikes={this.sortByLikes}
                         sortByDate={this.sortByDate}
                         sortByComments={this.sortByComments}
                         addSort={this.addSort}
                         amountLikeFilter={this.amountLikeFilter}
                         amountCountFilter={this.amountCountFilter}
                         amountDateFilter={this.amountDateFilter}
                         setAmount={this.setAmount}
                />
                <InfiniteScroll
                    pageStart={0}
                    loadMore={this.loadMorePosts}
                    hasMore={hasMorePosts}
                    loader={<Loader active inline="centered" key={0}/>}
                >
                    {postsFiltrate.map(post => (
                        <Post
                            post={post}
                            hidePost={this.props.hidePost}
                            reactPost={this.props.reactPost}
                            deletePost={props.deletePost}
                            toggleExpandedPost={props.toggleExpandedPost}
                            sharePost={this.sharePost}
                            key={post.id}
                            selfID={selfID}
                            user={this.props.user}
                            toggleUpdatePost={this.toggleUpdatePost}
                            toggleUpdateComment={this.toggleUpdateComment}

                        />
                    ))}
                </InfiniteScroll>
                {
                    expandedPost
                    && <ExpandedPost sharePost={this.sharePost} toggleUpdatePost={this.toggleUpdatePost}
                                     toggleUpdateComment={this.toggleUpdateComment}/>
                }
                {
                    sharedPostId
                    && <SharedPostLink postId={sharedPostId} selfMail={this.props.userMail}
                                       sharePost={this.props.sharePost} close={this.closeSharePost}/>
                }
                {
                    this.state.updatePost
                    && <UpdatePost post={this.state.updatePost} updatePost={this.props.updatePost}
                                   toggleUpdatePost={this.toggleUpdatePost}/>
                }
                {
                    this.state.updateComment
                    && <UpdateComment comment={this.state.updateComment}
                                      postIdForUpdateComment={this.state.postIdForUpdateComment}
                                      updateComment={this.props.updateComment}
                                      toggleUpdateComment={this.toggleUpdateComment}/>
                }
            </div>
        );
    }
}

Thread.propTypes = {
    posts: PropTypes.arrayOf(PropTypes.object),
    hasMorePosts: PropTypes.bool,
    expandedPost: PropTypes.objectOf(PropTypes.any),
    sharedPostId: PropTypes.string,
    userId: PropTypes.string,
    loadPosts: PropTypes.func.isRequired,
    loadMorePosts: PropTypes.func.isRequired,
    deletePost: PropTypes.func.isRequired,
    toggleExpandedPost: PropTypes.func.isRequired,
    addPost: PropTypes.func.isRequired,
    hidePost: PropTypes.func.isRequired,
};

Thread.defaultProps = {
    posts: [],
    hasMorePosts: true,
    expandedPost: undefined,
    sharedPostId: undefined,
    userId: undefined
};

const mapStateToProps = rootState => ({
    posts: rootState.posts.posts,
    hasMorePosts: rootState.posts.hasMorePosts,
    expandedPost: rootState.posts.expandedPost,
    userId: rootState.profile.user.id,
    userMail: rootState.profile.user.email,
    user: rootState.profile.user,
});

const actions = {
    loadPosts,
    loadMorePosts,
    hidePost,
    toggleExpandedPost,
    addPost,
    deletePost,
    sharePost,
    updatePost,
    reactPost,
    updateComment
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Thread);
