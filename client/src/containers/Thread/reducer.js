import {ADD_POST, DEL_POST, LOAD_MORE_POSTS, SET_ALL_POSTS, SET_EXPANDED_POST} from './actionTypes';
import {uniqPost} from '../../helpers/uniqPostHelper'

export default (state = {}, action) => {
    switch (action.type) {
        case SET_ALL_POSTS:
            return {
                ...state,
                posts: [...action.posts],
                hasMorePosts: Boolean(action.posts.length)
            };
        case LOAD_MORE_POSTS:
            return {
                ...state,
                posts: uniqPost([...(state.posts || []), ...action.posts]),
                hasMorePosts: Boolean(action.posts.length)
            };
        case ADD_POST:
            return {
                ...state,
                posts: [action.post, ...state.posts]
            };
        case SET_EXPANDED_POST:
            if (!action.post)
                return {
                    ...state,
                    expandedPost: undefined
                };
            return {
                ...state,
                expandedPost: {
                    ...action.post
                }
            };
        case 'updatePost':
            let all_post = state.posts;
            let length = all_post.length;
            let j = 0;
            for (j = 0; j < length; j++)
                if (all_post[j].id === action.post.id)
                    break;
            all_post[j] = action.post;
            return {
                ...state,
                posts: [...all_post]
            };
        case DEL_POST:
            let posts = state.posts;
            let i = 0;
            let length1 = posts.length;
            for (i = 0; i < length1; i++)
                if (posts[i].id === action.postId)
                    break;
            posts.splice(i, 1);
            return {
                ...state,
                posts: [...posts]
            };
        default:
            return state;
    }
};
