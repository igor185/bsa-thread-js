import * as authService from 'src/services/authService';
import * as profileService from 'src/services/profileService';
import {
    SET_USER,
    SET_IS_LOADING,
    UPDATE_STATUS, UPDATE_NAME
} from './actionTypes';

const setToken = token => localStorage.setItem('token', token);

const setUser = user => async dispatch => dispatch({
    type: SET_USER,
    user
});

const setIsLoading = isLoading => async dispatch => dispatch({
    type: SET_IS_LOADING,
    isLoading
});

const setAuthData = (user = null, token = '') => (dispatch, getRootState) => {
    setToken(token); // token should be set first before user
    setUser(user)(dispatch, getRootState);
};

const handleAuthResponse = authResponsePromise => async (dispatch, getRootState) => {
    const {
        user,
        token
    } = await authResponsePromise;
    setAuthData(user, token)(dispatch, getRootState);
};
export const postName = async (name, id) => await profileService.postName(name, id);

export const login = request => handleAuthResponse(authService.login(request));

export const registration = request => handleAuthResponse(authService.registration(request));

export const logout = () => setAuthData();

export const loadCurrentUser = () => async (dispatch, getRootState) => {
    setIsLoading(true)(dispatch, getRootState);
    const user = await authService.getCurrentUser();
    setUser(user)(dispatch, getRootState);
    setIsLoading(false)(dispatch, getRootState);
};

export const updateStatus = (status) => {
    return {
        type:UPDATE_STATUS,
        payload:{
            status
        }
    }
};
export const updateName = (name) => {
    return {
        type:UPDATE_NAME,
        payload:{
            name
        }
    }
};

