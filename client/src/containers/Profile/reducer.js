import {SET_IS_LOADING, SET_USER, UPDATE_STATUS, UPDATE_NAME} from './actionTypes';

export default (state = {}, action) => {
    switch (action.type) {
        case SET_USER:
            return {
                ...state,
                user: action.user,
                isAuthorized: Boolean(action.user && action.user.id)
            };
        case SET_IS_LOADING:
            return {
                ...state,
                isLoading: action.isLoading
            };
        case UPDATE_STATUS:
            const user = state.user;
            user.status = action.payload.status;
            return {
                ...state,
                user: {...user}
            };
        case UPDATE_NAME:
            const user1= state.user;
            user1.username = action.payload.name;
            return {
                ...state,
                user: {...user1}
            };
        default:
            return state;
    }
};
