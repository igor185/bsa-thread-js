import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getUserImgLink } from 'src/helpers/imageHelper';
import {
    Grid,
    Image,
    Input,
    Button,
    Popup,
    Icon
} from 'semantic-ui-react';
import { postName, postStatus, postImage } from '../../services/profileService'
import { NotificationManager } from 'react-notifications';
import * as imageService from 'src/services/imageService';
import {updateStatus, updateName} from "./actions";
import {bindActionCreators} from "redux";


class Profile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user: props.user,
            isValidName: true,
            isStatusValid: !!props.user.status,
            isImageValid: true,
            isUploading: false,
            imageId: undefined,
            imageLink: undefined,
            status: props.user.status
        };
    }

    render() {
        const { imageLink,  isUploading, imageId} = this.state;

        return (
            <Grid container textAlign="center" style={{ paddingTop: 30 }}>
                <Grid.Column>
                    <Image centered src={imageLink || getUserImgLink(this.state.user.image)} size="medium" circular />
                    <br />
                    <Input
                        id="userInput"
                        icon="user"
                        iconPosition="left"
                        placeholder="Username"
                        type="text"
                        defaultValue={this.state.user.username}
                        error={!this.state.isValidName}
                        onChange={(ev) => {
                            ev.target.value.trim() !== '' ? this.setState({ isValidName: true }) : this.setState({ isValidName: false })
                        }
                        }
                    />
                    <Popup content={"Change your username"} trigger={<Button icon="write" onClick={() => {
                        if (!this.state.isValidName)
                            return;
                        let name = document.getElementById('userInput').value;
                        if (name !== this.state.user.username)
                            postName(name, this.state.user.id)
                                .then(() => {
                                    NotificationManager.info('Please, reload page', `Your name was changed, ${name}`);
                                    this.props.updateName(name);
                                })
                                .catch((error) => {
                                    NotificationManager.error(error.message, 'Something go wrong!');
                                });
                    }} />} />
                    <br />
                    <br />
                    <Input
                        icon="at"
                        iconPosition="left"
                        placeholder="Email"
                        type="email"
                        disabled
                        value={this.state.user.email}
                    />

                    <br />
                    <br />
                    <Input
                        id="statusInput"
                        icon="talk"
                        iconPosition="left"
                        type="text"
                        placeholder="Write your status"
                        error={!this.state.isStatusValid}
                        value={this.state.status}
                        onChange={(ev) => {
                            ev.target.value.trim() !== '' ? this.setState({ isStatusValid: true, status:ev.target.value.trim()}) : this.setState({ isStatusValid: false })
                        }

                        }
                    />
                    <Popup content={"Change your status"} trigger={<Button icon="write" onClick={() => {
                        if (!this.state.isStatusValid)
                            return;
                        if (this.state.status !== this.state.user.status)
                            postStatus(this.state.status, this.state.user.id)
                                .then(() => {
                                    this.props.updateStatus(this.state.status);
                                })
                                .catch((error) => {
                                    NotificationManager.error(error.message, 'Something go wrong!');
                                });
                    }} />} />
                    <br />
                    <br />

                    <Button color="teal" icon labelPosition="left" as="label" loading={isUploading} disabled={isUploading}>
                        <Icon name="image" />
                        Attach image
                        <input name="image" type="file" hidden onChange={this.handleUploadFile} />
                    </Button>
                    <Popup content={"Change your image"} trigger={<Button icon="image" onClick={() => {
                        if (!imageId) return;
                        postImage(imageId, this.state.user.id)
                            .then(() => {
                                NotificationManager.info('Please, reload page', `Your image was changed, ${this.props.user.username}`);
                            })
                            .catch((error) => {
                                NotificationManager.error(error.message, 'Something go wrong!');
                            })
                    }} />} />
                </Grid.Column>
            </Grid>
        )
    }
    handleUploadFile = async ({ target }) => {
        this.setState({ isUploading: true });
        try {
            const { id: imageId, link: imageLink } = await imageService.uploadImage(target.files[0]);
            this.setState({ imageId, imageLink, isUploading: false });
        } catch {
            // TODO: show error
            this.setState({ isUploading: false });
        }
    }
}

Profile.propTypes = {
    user: PropTypes.objectOf(PropTypes.any)
};

Profile.defaultProps = {
    user: {}
};

const mapStateToProps = rootState => ({
    user: rootState.profile.user,
});
const mapDispatchToProps = dispatch => bindActionCreators({updateStatus, updateName}, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Profile);
