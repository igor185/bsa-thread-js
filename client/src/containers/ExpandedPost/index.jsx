import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal, Comment as CommentUI, Header } from 'semantic-ui-react';
import moment from 'moment';
import { reactPost, toggleExpandedPost, addComment, reactComment, hidePost, deletePost, deleteComment } from 'src/containers/Thread/actions';
import Post from 'src/components/Post';
import Comment from 'src/components/Comment';
import AddComment from 'src/components/AddComment';
import Spinner from 'src/components/Spinner';
import {filter} from '../../helpers/find.helpers'

class ExpandedPost extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: true
        };
    }

    closeModal = () => {
        // this.setState({ open: false })
        this.props.toggleExpandedPost();
    }
    deletePost = (...args) => {
        this.closeModal();
        this.props.deletePost(...args);
    }

    render() {
        const { post, sharePost, ...props } = this.props;
        const {likes, dislikes} = filter(post.react);
        post.likes=likes;
        post.dislikes=dislikes;
        return (
            <Modal centered={false} open={this.state.open} onClose={this.closeModal}>
                {post
                    ? (
                        <Modal.Content>
                            <Post
                                post={post}
                                reactPost={props.reactPost}
                                toggleExpandedPost={props.toggleExpandedPost}
                                sharePost={sharePost}
                                selfID={this.props.userId}
                                user={this.props.user}
                                toggleUpdatePost={this.props.toggleUpdatePost}
                                hidePost={this.props.hidePost}
                                deletePost={this.deletePost}
                            />
                            <CommentUI.Group style={{ maxWidth: '100%' }}>
                                <Header as="h3" dividing>
                                    Comments
                                </Header>
                                {post.comments && post.comments
                                    .sort((c1, c2) => moment(c1.createdAt).diff(c2.createdAt))
                                    .map(comment => <Comment key={comment.id} postId={post.id} toggleUpdateComment={props.toggleUpdateComment} comment={comment} reactComment={this.props.reactComment} selfId={this.props.userId} deleteComment={this.props.deleteComment} />)
                                }
                                <AddComment postId={post.id} addComment={props.addComment} />
                            </CommentUI.Group>
                        </Modal.Content>
                    )
                    : <Spinner />
                }
            </Modal>
        );
    }
}

ExpandedPost.propTypes = {
    post: PropTypes.objectOf(PropTypes.any).isRequired,
    toggleExpandedPost: PropTypes.func.isRequired,
    addComment: PropTypes.func.isRequired,
    sharePost: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
    post: rootState.posts.expandedPost,
    userId: rootState.profile.user.id,
    user: rootState.profile.user,
});
const actions = { toggleExpandedPost, addComment, reactComment, reactPost, hidePost, deletePost, deleteComment };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ExpandedPost);
