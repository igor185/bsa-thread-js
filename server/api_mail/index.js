import {send} from './send'

const resetUrl = (token) => `http://localhost:3001/reset/${token}`;
export const like = async (email) => {
    const msg = {
        to: email,
        from: process.env.USER_MAIL,
        subject: 'New like',
        text: `Hi! Your post was liked!`,
        html: `<strong>Hi! Your post was liked!</strong>`,
    };
    return await send(msg);
};

export const share = async ({ getMail, sendMail,postHash}) => {
    const msg = {
        to: getMail,
        from: process.env.USER_MAIL,
        subject: 'Share post',
        text: `Hi! The user ${sendMail} wants to share with you this post: ${postHash}`,
        html: `<strong>Hi! The user ${sendMail} wants to share with you this <a href="${postHash}">post</a></strong>`,
    };
    return await send(msg);
};

export const reset = async (email, token) => {
    const msg = {
        to: email,
        from: process.env.USER_MAIL,
        subject: 'Reset password',
        html: `<p>You requested for a password reset, kindly use this <a href="${resetUrl(token)}">link</a> to reset your password</p>`,
    };
    return await send(msg);
};
