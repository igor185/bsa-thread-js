export const findByKey = (arr, key, value) => {
    for (let i = 0, length = arr.length; i < length; i++)
        if (arr[i][key] === value)
            return i;
    return -1;
};

export const filter = arr => {
    const likes = [],
        dislikes = [];
    if (arr)
        arr.forEach(elem => {
            if (elem.id)
                elem.isLike ? likes.push(elem) : dislikes.push(elem)
        });
    return {
        likes,
        dislikes
    }
};
