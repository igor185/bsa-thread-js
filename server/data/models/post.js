export default (orm, DataTypes) => {
    return orm.define('post', {
        body: {
            allowNull: false,
            type: DataTypes.TEXT
        },
        createdAt: DataTypes.DATE,
        updatedAt: DataTypes.DATE,
        hide: DataTypes.BOOLEAN,
        delete: DataTypes.BOOLEAN,
    }, {});
};
