export default {
    up: (queryInterface, Sequelize) => queryInterface.sequelize
        .transaction(transaction => Promise.all([
            queryInterface.addColumn('users', 'reset_token', {
                type: Sequelize.STRING,
                defaultValue: null,
            }, { transaction }),
        ])),
    down: queryInterface => queryInterface.sequelize
        .transaction(transaction => Promise.all([
            queryInterface.removeColumn('user', 'reset_token', { transaction }),
        ]))
};
