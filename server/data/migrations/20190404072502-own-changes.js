export default {
    up: (queryInterface, Sequelize) => queryInterface.sequelize
        .transaction(transaction => Promise.all([
            queryInterface.addColumn('users', 'status', {
                type: Sequelize.STRING,
                allowNull: true,
            }, { transaction }),
            queryInterface.addColumn('posts', 'hide', {
                type: Sequelize.BOOLEAN,
                defaultValue: false,
            }, { transaction }),
            queryInterface.addColumn('posts', 'delete', {
                type: Sequelize.BOOLEAN,
                defaultValue: false,
            }, { transaction }),
            queryInterface.addColumn('commentReactions', 'commentId', {
                type: Sequelize.UUID,
                references: {
                    model: 'comments',
                    key: 'id',
                },
                onUpdate: 'CASCADE',
                onDelete: 'SET NULL',
            }, { transaction }),
            queryInterface.addColumn('commentReactions', 'userId', {
                type: Sequelize.UUID,
                references: {
                    model: 'users',
                    key: 'id',
                },
                onUpdate: 'CASCADE',
                onDelete: 'SET NULL',
            }, { transaction }),
        ])),
    down: queryInterface => queryInterface.sequelize
        .transaction(transaction => Promise.all([
            queryInterface.removeColumn('users', 'status', { transaction }),
            queryInterface.removeColumn('posts', 'hide', { transaction }),
            queryInterface.removeColumn('posts', 'delete', { transaction }),
            queryInterface.removeColumn('commentReactions', 'commentId', { transaction }),
            queryInterface.removeColumn('commentReactions', 'userId', { transaction }),
        ]))
};
