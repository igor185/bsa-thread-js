import {PostModel, PostReactionModel, UserModel} from '../models/index';
import BaseRepository from './base.repository';
import sequelize from '../db/connection';

class PostReactionRepository extends BaseRepository {

    getPostReactions(postId) {
        return this.model.findAll({
            group: [
                'postReaction.id',
                'post.id',
                'postReaction.userId',
                'user.id',
            ],
            where: {
                postId,
            },
            include: [{
                model: PostModel,
                attributes: ['id', 'userId']
            },
                {
                    model: UserModel,
                    attributes: ['id']
                },
            ],
            raw: true,
            attributes: {
                include: [
                    [sequelize.literal(`
                (SELECT link
                FROM "images" as "im"
                WHERE "user"."imageId" = "im"."id")`), 'link']
                ]
            }
        });
    }
}

export default new PostReactionRepository(PostReactionModel);
