import {
    CommentModel,
    UserModel,
    ImageModel
} from '../models/index';
import BaseRepository from './base.repository';
import commentReactions from './commentReactins.repository';


class CommentRepository extends BaseRepository {
    async getCommentById(id) {
        let res = await this.model.findOne({
            group: [
                'comment.id',
                'user.id',
                'user->image.id'
            ],
            where: {
                id
            },
            include: [{
                model: UserModel,
                attributes: ['id', 'username'],
                include: {
                    model: ImageModel,
                    attributes: ['id', 'link']
                }
            }]
        });
        res = JSON.parse(JSON.stringify(res));
        res.react = await commentReactions.getCommentReactions(id);

        return res;
    }
}

export default new CommentRepository(CommentModel);
