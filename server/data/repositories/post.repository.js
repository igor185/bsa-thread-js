import {Op} from 'sequelize';
import {CommentModel, ImageModel, PostModel, UserModel,} from '../models/index';
import BaseRepository from './base.repository';
import sequelize from '../db/connection';


class PostRepository extends BaseRepository {
    async getPosts(filter, selfId) {
        const {from: offset, count: limit, userId, deletePost = false} = filter;

        const where = {
            [Op.or]: [{
                userId: {
                    [Op.eq]: selfId
                }
            },
                {
                    hide: {
                        [Op.eq]: false
                    }
                }
            ]
        };

        if (userId)
            Object.assign(where, {userId});

        if(deletePost)
            Object.assign(where, {delete: true});

        return this.model.findAll({
            where: {
                ...where,
                delete: deletePost
            },
            attributes: {
                include: [
                    [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId")`), 'commentCount'],
                ]
            },
            include: [{
                model: ImageModel,
                attributes: ['id', 'link']
            }, {
                model: UserModel,
                attributes: ['id', 'username', 'email', 'status'],
                include: {
                    model: ImageModel,
                    attributes: ['id', 'link']
                }
            }],
            group: [
                'post.id',
                'image.id',
                'user.id',
                'user->image.id',
            ],
            order: [
                ['createdAt', 'DESC']
            ],
            offset,
            limit,
        });
    }

    getPostById(id) {
        return this.model.findOne({
            group: [
                'post.id',
                'comments.id',
                'comments->user.id',
                'comments->user->image.id',
                'user.id',
                'user->image.id',
                'image.id'
            ],
            where: {
                id
            },
            attributes: {
                include: [
                    [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId")`), 'commentCount']
                ]
            },
            include: [{
                model: CommentModel,
                include: {
                    model: UserModel,
                    attributes: ['id', 'username', 'status'],
                    include: {
                        model: ImageModel,
                        attributes: ['id', 'link']
                    }
                }
            }, {
                model: UserModel,
                attributes: ['id', 'username'],
                include: {
                    model: ImageModel,
                    attributes: ['id', 'link']
                }
            }, {
                model: ImageModel,
                attributes: ['id', 'link']
            }]
        });
    }

    deletePost({postId}) {
        return this.deleteById(postId);
    }
}

export default new PostRepository(PostModel);
