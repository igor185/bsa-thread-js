import {
    UserModel,
    ImageModel
} from '../models/index';
import BaseRepository from './base.repository';
import sequelize from '../db/connection';

class UserRepository extends BaseRepository {
    addUser(user) {
        return this.create(user);
    }

    getByEmail(email) {
        return this.model.findOne({
            where: {
                email
            }
        });
    }
    getByToken(token) {
        return this.model.findOne({
            where: {
                reset_token: token
            }
        });
    }

    getByUsername(username) {
        return this.model.findOne({
            where: {
                username
            }
        });
    }

    getUserById(id) {
        return this.model.findOne({
            group: [
                'user.id',
                'image.id'
            ],
            where: {
                id
            },
            include: {
                model: ImageModel,
                attributes: ['id', 'link']
            },
            attributes: ['id', 'username', 'status', 'imageId', 'email']
        });
    }

    postName(name, id) {
        return sequelize.query(`UPDATE users SET username = '${name}' WHERE id = '${id}'`);
    }

    postStatus(status, id) {
        return sequelize.query(`UPDATE users SET status = '${status}' WHERE id = '${id}'`);
    }

    postImage(imageId, userId) {
        return this.updateById(userId, {
            imageId
        });
    }
}

export default new UserRepository(UserModel);
