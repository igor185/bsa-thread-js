import {
    CommentReactionsModel,
    UserModel,
} from '../models/index';
import BaseRepository from './base.repository';
import sequelize from '../db/connection';

class CommentReactionsRepository extends BaseRepository {
    getCommentReactions(commentId) {
        return this.model.findAll({
            group: [
                'commentReactions.id',
                'commentReactions.userId',
                'user.id',
            ],
            where: {
                commentId,
            },
            include: [{
                model: UserModel,
                attributes: ['id']
            }, ],
            raw: true,
            attributes: {
                include: [
                    [sequelize.literal(`
                (SELECT link
                FROM "images" as "im"
                WHERE "user"."imageId" = "im"."id")`), 'link']
                ]
            }
        });
    }
}

export default new CommentReactionsRepository(CommentReactionsModel);
