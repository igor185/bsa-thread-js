export default (models) => {
    const {
        User,
        Post,
        PostReaction,
        Comment,
        Image,
        commentReactions
    } = models;


    Image.hasOne(User);
    Image.hasOne(Post);

    User.hasMany(Post);
    User.hasMany(Comment);
    User.hasMany(PostReaction);
    User.belongsTo(Image);
    User.hasMany(commentReactions);

    Post.belongsTo(Image);
    Post.belongsTo(User);
    Post.hasMany(PostReaction);
    Post.hasMany(Comment);

    Comment.belongsTo(User);
    Comment.belongsTo(Post);
    Comment.hasMany(commentReactions);

    PostReaction.belongsTo(Post);
    PostReaction.belongsTo(User);

    commentReactions.belongsTo(Comment);
    commentReactions.belongsTo(User);
};
