export default [
    '/auth/login',
    '/auth/register',
    '/auth/reset',
    '/auth/reset/token'
];
