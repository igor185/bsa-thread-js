import {
    Router
} from 'express';
import * as commentService from '../services/comment.service';

const router = Router();

router
    .post('/', (req, res, next) => commentService.create(req.user.id, req.body)
        .then(comment => res.send(comment))
        .catch(next))
    .post('/react', (req, res, next) => commentService.reaction(req.body)
        .then((message) => {
            res.send(message);
        })
        .catch(next))
    .put('/:id', (req, res, next) => commentService.UpdateCommentBody(req.params.id, req.body.comment)
        .then(() => res.send(req.body.comment))
        .catch(next))
    .delete('/:id', (req, res, next) => commentService.deleteComment(req.params.id)
        .then((data) => res.send(data))
        .catch(next));

export default router;
