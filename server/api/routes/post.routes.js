import {
    Router
} from 'express';
import * as postService from '../services/post.service';
import * as mailService from '../../api_mail/index'

const router = Router();

router
    .get('/', (req, res, next) => postService.getPosts(req.query, req.query.selfId)
        .then((posts) => {
            // console.log(req.query);
            res.send(posts);
        })
        .catch(next))
    .get('/:id', (req, res, next) => postService.getPostById(req.params.id)
        .then(post => res.send(post))
        .catch(next))
    .put('/reaction', (req, res, next) => postService.setReaction(req.body.user.id, req.body) // user added to the request in the jwt strategy, see passport config
        .then(reaction => {
            if (req.body.post_userId !== req.user.id) {
                if (reaction.isLike === true) {
                    mailService.like(req.body.user.email)
                        .catch(e => console.log(e.message));
                    req.io.to(req.body.post_userId).emit('like', 'Your post was liked!');
                } else {
                    req.io.to(req.body.post_userId).emit('like', 'Your post was disliked!');
                }
            }
            req.io.emit('reaction', {
                reaction,
                postId: req.body.postId,
                userId: req.body.user.id,
            });
            return res.send(reaction);
        })
        .catch(next))
    .put('/:id', (req, res, next) => postService.UpdatePostBody(req.params.id, req.body.post)
        .then(() => res.send(req.body.post))
        .catch(next))
    .post('/', (req, res, next) => postService.create(req.user.id, req.body) // user added to the request in the jwt strategy, see passport config
        .then((post) => {
            req.io.emit('new_post', post); // notify all users that a new post was created
            return res.send(post);
        })
        .catch(next))
    .put('/', (req, res, next) => {postService.updateById(req.body.postId, {hide:req.body.data.hide})
            .then(() => {
                if(req.body.data.hide !== undefined && req.body.data.hide === true){
                    req.io.emit('hide_post', {
                        postId:req.body.postId,
                        userId:req.body.data.userId
                    });
                }
            })
            .catch(next);
    })
    .delete('/:id', (req, res, next) => postService.delPostById(req.params.id, req.query.deletePost)
        .then(() => {
            req.io.emit('delete_post', {
                postId: req.params.id,
            });
            res.send({
                postId: req.params.id,
            });
        })
        .catch(next))
    .get('/react/:postId/:isLike', (req, res, next) => postService.getPostReactions(req.params.postId, req.params.isLike)
        .then((data) => {
            res.send(data);
        })
        .catch(next))
    .post('/share', (req, res, next) => mailService.share(req.body)
        .then(() => res.status(200).send())
        .catch((error) => {
            console.log(error);
            next();
        }));

export default router;
