import postRepository from '../../data/repositories/post.repository';
import postReactionRepository from '../../data/repositories/post-reaction.repository';
import commentReactionRepository from '../../data/repositories/commentReactins.repository';
import {findByKey} from "../../helpers/find.helpers";


export const getPostReactions = async (postId, isLike) => {
    const reactions = (await postReactionRepository.getPostReactions(postId, isLike));
    return reactions;
};

export const getPosts = async (filter, userId) => {
    let posts = await postRepository.getPosts(filter, userId);
    posts = JSON.parse(JSON.stringify(posts));

    posts = await Promise.all(posts.map(async (post) => {
        const reaction = await getPostReactions(post.id);
        post.react = reaction;

        return post;
    }));
    return posts;
};

export const getPostById = async id => {
    let post = await postRepository.getPostById(id);
    post = await JSON.parse(JSON.stringify(post));
    post.react = await getPostReactions(post.id);

    post.comments = await Promise.all(post.comments.map(async (comment) => {
        comment.react = await commentReactionRepository.getCommentReactions(comment.id);

        return comment;
    }));
    return post;
};

export const create = (userId, post) => postRepository.create({
    ...post,
    userId
});

export const UpdatePostBody = async (id, {
    body
}) => await postRepository.updateById(id, {
    body
});

export const delPostById = async (postId, deletePost = true) => {
    return await postRepository.updateById(postId, {
        delete: deletePost
    });
};
export const createReaction = async (userId, {postId, isLike = true}) => {
    await postReactionRepository.create({postId, userId, isLike});
    const post = await getPostById(postId);
    return post.react;
};

export const setReaction = async (userId, {postId, isLike = true}) => {
    let reactions = await postReactionRepository.getPostReactions(postId);
    reactions = await JSON.parse(JSON.stringify(reactions));

    const index = findByKey(reactions, 'userId', userId);

    if (index === -1)
        return await createReaction(userId, {postId, isLike});

    const reaction = reactions[index];

    if (reaction.isLike === isLike) {
        await postReactionRepository.deleteById(reaction.id);
        reactions.splice(index, 1);
        return reactions;
    }
    await postReactionRepository.updateById(reaction.id, {
        isLike: isLike
    });
    reactions[index].isLike = isLike;
    return reactions;
};

export const updateById = async (postId, data) => await postRepository.updateById(postId, data);
