import userRepository from '../../data/repositories/user.repository';
import crypto from 'crypto';
import {reset} from '../../api_mail/index';

export const getUserById = async (userId) => {
    const {id, username, email, imageId, image, status} = await userRepository.getUserById(userId);
    return {id, username, email, imageId, image, status};
};

export const postName = (name, id) => userRepository.postName(name, id);


export const postStatus = (status, id) => userRepository.postStatus(status, id);


export const postImage = (image, id) => userRepository.postImage(image, id);


export const resetPassword = async (email) =>{
    console.log(email);
    const user = await userRepository.getByEmail(email);
    if(!user)
        throw new Error('Not found');
    const token = (await crypto.randomBytes(20)).toString('hex');
    await userRepository.updateById(user.id, {reset_token:token});
    await reset(email, token);
};
