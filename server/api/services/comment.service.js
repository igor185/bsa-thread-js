import commentRepository from '../../data/repositories/comment.repository';
import commentReactions from '../../data/repositories/commentReactins.repository';
import {findByKey} from "../../helpers/find.helpers";

export const create = async (userId, comment) => {
    const {id} = await commentRepository.create({...comment, userId});
    return await getCommentById(id);
};


export const getCommentById = async id => await commentRepository.getCommentById(id);

export const createReaction = async ({commentId, userId, react}) => {
    await commentReactions.create({commentId, userId, isLike: react});
    let comment = await getCommentById(commentId);
    return comment.react;
};

export const reaction = async ({commentId, userId, react}) => {
    let reactions = await commentReactions.getCommentReactions(commentId);
    reactions = await JSON.parse(JSON.stringify(reactions));

    const index = findByKey(reactions, 'userId', userId);

    if (index === -1)
        return await createReaction({commentId, userId, react});

    const reaction = reactions[index];

    if (reaction.isLike === react) {
        await commentReactions.deleteById(reaction.id);
        reactions.splice(index, 1);
        return reactions;
    }
    await commentReactions.updateById(reactions[index].id, {isLike: react});

    reactions[index].isLike = react;
    return reactions;
};
export const UpdateCommentBody = async (id, {
    body
}) => await commentRepository.updateById(id, {
    body
});

export const deleteComment = async id => await commentRepository.updateById(id, {delete: true});
