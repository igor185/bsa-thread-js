import tokenHelper from '../../helpers/token.helper';
import cryptoHelper from '../../helpers/crypto.helper';
import userRepository from '../../data/repositories/user.repository';

export const login = async ({ id }) => ({
    token: tokenHelper.createToken({ id }),
    user: await userRepository.getUserById(id)
});

export const register = async ({ password, ...userData }) => {
    const newUser = await userRepository.addUser({
        ...userData,
        password: await cryptoHelper.encrypt(password)
    });
    return login(newUser);
};

export const reset = async (token, password) => {

    console.log(token, password);
    const user = await userRepository.getByToken(token);

    if(!user)
        throw new Error('No valid token');

    const cryptoPassword = await cryptoHelper.encrypt(password);

    await userRepository.updateById(user.id, {password: cryptoPassword});
};
